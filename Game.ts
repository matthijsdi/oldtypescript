/**
 * Created by Matthijs on 23-5-2019.
 */
window.onload = function() {
    main()
    createFood();
    createDeath();
    document.addEventListener("keydown", changeDirection);
};

const GAME_SPEED = 100;
const DEATH_BLOCKS = 10;
const gameCanvas = <HTMLCanvasElement> document.getElementById("game");
var ctx = gameCanvas.getContext("2d");

let snake = [
    {x: 150, y: 150},
    {x: 140, y: 150},
    {x: 130, y: 150},
    {x: 120, y: 150},
    {x: 110, y: 150}
]
let speedX = 10;
let speedY = 0;
let foodX;
let foodY;
let death= [
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100},
    {x: 100, y: 100}
]

function main(){
    if (didGameEnd()) {
        alert('Gameover')
        return;
    };

    setTimeout(function onTick(){
        clearCanvas();
        drawFood();
        drawDeath();
        advanceSnake();
        drawSnake();
        main();
    }, GAME_SPEED)

}

function createFood() {
    var max = 0;
    var minX = gameCanvas.width -10;
    var minY = gameCanvas.height -10;

    foodX = Math.round((Math.random() * (max-minX) + minX) / 10) * 10;

    foodY = Math.round((Math.random() * (max-minY) + minY) / 10) * 10;

    snake.forEach(function isFoodOnSnake(part) {
        var foodIsoNsnake = part.x == foodX && part.y == foodY;
        if (foodIsoNsnake) createFood();
    });
}

function drawFood() {
    ctx.fillStyle = 'yellow';
    ctx.strokeStyle = 'black';

    ctx.fillRect(foodX, foodY, 10, 10);
    ctx.strokeRect(foodX, foodY, 10, 10);
}

function createDeath() {
    for (let i = 0; i < DEATH_BLOCKS; i++) {
        var max = 0;
        var minX = gameCanvas.width - 10;
        var minY = gameCanvas.height - 10;

        death[i].x = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
        death[i].y = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;

        snake.forEach(function isDeathOnSnake(part) {
            if (part.x == death[i].x && part.y == death[i].y || part.x == foodX && part.y == foodY)
                createDeath();
        });
    }
}

function drawDeath() {
    for (let i=0; i < DEATH_BLOCKS; i++) {
        ctx.fillStyle = 'pink';
        ctx.strokeStyle = 'black';

        ctx.fillRect(death[i].x, death[i].y, 10, 10);
        ctx.strokeRect(death[i].x, death[i].y, 10, 10);
    }
}

function clearCanvas() {
    ctx.fillStyle = 'white';
    ctx.strokeStyle = 'black';

    ctx.fillRect(0, 0, gameCanvas.width, gameCanvas.height);
    ctx.strokeRect(0, 0, gameCanvas.width, gameCanvas.height);
}

function advanceSnake() {
    const head = {x: snake[0].x + speedX, y: snake[0].y + speedY};;

    snake.unshift(head);
    if (snake[0].x === foodX && snake[0].y === foodY) {
        createFood();
        createDeath()
    } else {
        snake.pop();
    }
}

function drawSnake() {
    snake.forEach(drawSnakePart)
}

function drawSnakePart(snakePart) {
    ctx.fillStyle = ctx.strokeStyle = 'blue';

    ctx.fillRect(snakePart.x, snakePart.y, 10, 10);
    ctx.strokeRect(snakePart.x, snakePart.y, 10, 10);
}

function didGameEnd() {
    for (let i = 4; i < snake.length; i++) {
        if (// Raakt de slang zichzelf?
            snake[i].x === snake[0].x && snake[i].y === snake[0].y
            // Raakt de slang de linker of de bovenste muur?
            || snake[0].x < 0                       || snake[0].y < 0
            // Raakt de slang de rechter of onderste muur?
            || snake[0].x > gameCanvas.width - 10   || snake[0].y > gameCanvas.height - 10){
            return true
        } else {
            for(let i = 0; i<DEATH_BLOCKS; i++){
                if (snake[0].x == death[i].x && snake[0].y == death[i].y){
                    return true;
                }
            }
            return false
        }
    }
}

function changeDirection(event) {
    const LEFT_KEY = 37;
    const RIGHT_KEY = 39;
    const UP_KEY = 38;
    const DOWN_KEY = 40;


    const keyPressed = event.keyCode;
    const goingUp = speedY === -10;
    const goingDown = speedY === 10;
    const goingRight = speedX === 10;
    const goingLeft = speedX === -10;

    if (keyPressed === LEFT_KEY && !goingRight) {
        speedX = -10;
        speedY = 0;
    }
    if (keyPressed === UP_KEY && !goingDown) {
        speedX = 0;
        speedY = -10;
    }
    if (keyPressed === RIGHT_KEY && !goingLeft) {
        speedX = 10;
        speedY = 0;
    }
    if (keyPressed === DOWN_KEY && !goingUp) {
        speedX = 0;
        speedY = 10;
    }
}

