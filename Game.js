/**
 * Created by Matthijs on 23-5-2019.
 */
window.onload = function () {
    main();
    createFood();
    createDeath();
    document.addEventListener("keydown", changeDirection);
};
var GAME_SPEED = 100;
var DEATH_BLOCKS = 10;
var gameCanvas = document.getElementById("game");
var ctx = gameCanvas.getContext("2d");
var snake = [
    { x: 150, y: 150 },
    { x: 140, y: 150 },
    { x: 130, y: 150 },
    { x: 120, y: 150 },
    { x: 110, y: 150 }
];
var speedX = 10;
var speedY = 0;
var foodX;
var foodY;
var death = [
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 },
    { x: 100, y: 100 }
];
function main() {
    if (didGameEnd()) {
        alert('Gameover');
        return;
    }
    ;
    setTimeout(function onTick() {
        clearCanvas();
        drawFood();
        drawDeath();
        advanceSnake();
        drawSnake();
        main();
    }, GAME_SPEED);
}
function createFood() {
    var max = 0;
    var minX = gameCanvas.width - 10;
    var minY = gameCanvas.height - 10;
    foodX = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
    foodY = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;
    snake.forEach(function isFoodOnSnake(part) {
        var foodIsoNsnake = part.x == foodX && part.y == foodY;
        if (foodIsoNsnake)
            createFood();
    });
}
function drawFood() {
    ctx.fillStyle = 'yellow';
    ctx.strokeStyle = 'black';
    ctx.fillRect(foodX, foodY, 10, 10);
    ctx.strokeRect(foodX, foodY, 10, 10);
}
function createDeath() {
    var _loop_1 = function (i) {
        max = 0;
        minX = gameCanvas.width - 10;
        minY = gameCanvas.height - 10;
        death[i].x = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
        death[i].y = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;
        snake.forEach(function isDeathOnSnake(part) {
            if (part.x == death[i].x && part.y == death[i].y || part.x == foodX && part.y == foodY)
                createDeath();
        });
    };
    var max, minX, minY;
    for (var i = 0; i < DEATH_BLOCKS; i++) {
        _loop_1(i);
    }
}
function drawDeath() {
    for (var i = 0; i < DEATH_BLOCKS; i++) {
        ctx.fillStyle = 'pink';
        ctx.strokeStyle = 'black';
        ctx.fillRect(death[i].x, death[i].y, 10, 10);
        ctx.strokeRect(death[i].x, death[i].y, 10, 10);
    }
}
function clearCanvas() {
    ctx.fillStyle = 'white';
    ctx.strokeStyle = 'black';
    ctx.fillRect(0, 0, gameCanvas.width, gameCanvas.height);
    ctx.strokeRect(0, 0, gameCanvas.width, gameCanvas.height);
}
function advanceSnake() {
    var head = { x: snake[0].x + speedX, y: snake[0].y + speedY };
    ;
    snake.unshift(head);
    if (snake[0].x === foodX && snake[0].y === foodY) {
        createFood();
        createDeath();
    }
    else {
        snake.pop();
    }
}
function drawSnake() {
    snake.forEach(drawSnakePart);
}
function drawSnakePart(snakePart) {
    ctx.fillStyle = ctx.strokeStyle = 'blue';
    ctx.fillRect(snakePart.x, snakePart.y, 10, 10);
    ctx.strokeRect(snakePart.x, snakePart.y, 10, 10);
}
function didGameEnd() {
    for (var i = 4; i < snake.length; i++) {
        if (snake[i].x === snake[0].x && snake[i].y === snake[0].y
            || snake[0].x < 0 || snake[0].y < 0
            || snake[0].x > gameCanvas.width - 10 || snake[0].y > gameCanvas.height - 10) {
            return true;
        }
        else {
            for (var i_1 = 0; i_1 < DEATH_BLOCKS; i_1++) {
                if (snake[0].x == death[i_1].x && snake[0].y == death[i_1].y) {
                    return true;
                }
            }
            return false;
        }
    }
}
function changeDirection(event) {
    var LEFT_KEY = 37;
    var RIGHT_KEY = 39;
    var UP_KEY = 38;
    var DOWN_KEY = 40;
    var keyPressed = event.keyCode;
    var goingUp = speedY === -10;
    var goingDown = speedY === 10;
    var goingRight = speedX === 10;
    var goingLeft = speedX === -10;
    if (keyPressed === LEFT_KEY && !goingRight) {
        speedX = -10;
        speedY = 0;
    }
    if (keyPressed === UP_KEY && !goingDown) {
        speedX = 0;
        speedY = -10;
    }
    if (keyPressed === RIGHT_KEY && !goingLeft) {
        speedX = 10;
        speedY = 0;
    }
    if (keyPressed === DOWN_KEY && !goingUp) {
        speedX = 0;
        speedY = 10;
    }
}
